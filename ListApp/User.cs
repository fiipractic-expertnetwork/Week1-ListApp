﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListApp
{
    public class User
    {
        // prop+TAB+TAB => public int MyProperty { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }

        //ctor+TAB+TAB => a new empty constructor
        public User()
        {

        }

        public User(string username, string password, string email, int age)
        {
            Username = username;
            Password = password;
            Email = email;
            Age = age;
        }
    }
}
