# Week 1

What we did at the end of the training.

## It contains:

  - Example of UserService and User class
  - The diferent sintaxes of [Linq](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/linq/query-syntax-and-method-syntax-in-linq) 
  - Commented commands and basic c# sintax

## Authors

* **Ciubotariu Florin-Constantin** - (ciubotariu.florin@gmail.com)
* **Moniry-Abyaneh Daniel** - (daniel.moniry@gmail.com)
