﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ListApp
{
    public class UserService
    {
        // prop+TAB+TAB => public int MyProperty { get; set; }
        public List<User> Users { get; set; }

        //ctor+TAB+TAB => a new empty constructor
        public UserService(List<User> users)
        {
            Users = users;
        }

        public List<User> GetUsersOver18For()
        {
            var usersOver18 = new List<User>();
            foreach (var user in Users)
            {
                if (user.Age >= 18)
                {
                    usersOver18.Add(user);
                }
            }
            return usersOver18;
        }

        public List<User> GetUsersOver18Linq()
        {
            // Results using Linq Query Sintax
            var resultQuerySintax = (from user in Users
                                    where user.Age >= 18
                                    select user).ToList();

            // Results using Linq Method Sintax
            var resultMethodSintax = Users.Where(user => user.Age >= 18).ToList();

            return resultMethodSintax; // or resultQuerySintax, they should be the same
        }

        public User GetUsersWithPasswordStartingWithSomething(string startingWithString)
        {

            // Results using Linq Query Sintax
            var resultQuerySintax = (from user in Users
                                     where user.Password.StartsWith(startingWithString)
                                     select user).FirstOrDefault();

            // Results using Linq Method Sintax
            var resultMethodSintax = Users.FirstOrDefault(user => user.Password.StartsWith(startingWithString));

            return resultQuerySintax; // or resultMethodSintax, they should be the same
        }

    }
}
