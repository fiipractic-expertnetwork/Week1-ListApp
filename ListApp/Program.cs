﻿using System;
using System.Collections.Generic;

namespace ListApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //CTRL+K+C - Comment lines
            //CTRL+K+U - Decomment lines
            //CTRL+K+D - Formats document
            var user1 = new User();// using the empty constructor and assigning the values
            user1.Username = "gigi";
            user1.Email = "randomEmail";
            user1.Password = "verystrongpassword";
            user1.Age = 20;

            var user2 = new User("Gigel2", "parola123", "gigi420@gmail.com", 18);//using the custom constructor that initializes 
            var user3 = new User // using the object initializer
            {
                Username = "Gigel3",
                Password = "parola345",
                Email = "test.lalal@sdsadas.com",
                Age = 7
            };

            var userList = new List<User>();
            userList.Add(user1);
            userList.Add(user2);
            userList.Add(user3);

            var userService = new UserService(userList);//declaring the user service and giving it the list of users as a dependency

            var usersOver18 = userService.GetUsersOver18Linq();// using a method from the service
            usersOver18.ForEach(user => Console.WriteLine(user.Username));// displaying all the Usernames in the usersOver18 list
            Console.WriteLine();//print empty line

            var userHavingPasswordStartingWithSpecificString = userService.GetUsersWithPasswordStartingWithSomething("parola");
            Console.WriteLine(userHavingPasswordStartingWithSpecificString.Username);
        }
    }
}
